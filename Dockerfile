FROM ubuntu:16.04

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 &&\
    echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list &&\
    apt update &&\
    apt install -y mongodb &&\
    apt install composer

RUN service mongodb start

RUN mkdir /var/www/communect /var/www/communect/modules

RUN /var/www/communect &&\
    git clone https://github.com/pixelhumain/pixelhumain

RUN cd /var/www/communect/modules/ &&\
    git clone https://github.com/pixelhumain/citizenToolKit &&\
    git clone https://github.com/pixelhumain/co2 &&\
    git clone https://github.com/pixelhumain/network &&\
    git clone https://github.com/pixelhumain/api &&\
    git clone https://github.com/pixelhumain/opendata &&\
    git clone https://github.com/pixelhumain/cityData

RUN cd /var/www/communect/pixelhumain/ph/ &&\
    composer update &&\
    composer install &&\
    mv protected/config/dbconfig.exemple.php protected/config/dbconfig.php


